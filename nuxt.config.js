module.exports = {
  cache: true,
  css: [
    '@/assets/fonts.scss',
    '@/assets/normalize.scss',
    '@/assets/global.scss',
    '@/assets/animations.scss'
  ],
  modules: [
    '@nuxtjs/axios'
  ],
  axios: {
    baseURL: 'http://localhost:8000/'
  },
  plugins: [
    // ssr: false to only include it on client-side
    {
      src: '~/plugins/VueCarousel.js',
      ssr: false
    },
    {
      src: '~/plugins/VueTouch.js',
      ssr: false
    },
    {
      src: '~plugins/RouterSync.js'
    },
    {
      src: '~plugins/NoSsr.js'
    },
    {
      src: '~plugins/Validate.js'
    }
  ],
  transition: {
    name: 'landingNext',
    mode: 'in-out',
    afterLeave (el) {
      this.$store.commit('scroll/animationEnd')
    }
  },
  /*
   ** Headers of the page
   */
  head: {
    title: 'spa-nekoneko',
    meta: [{
      charset: 'utf-8'
    },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: 'Nuxt.js project'
      }
    ],
    link: [{
      rel: 'icon',
      type: 'image/x-icon',
      href: '/favicon.ico'
    }]
  },
  /*
   ** Customize the progress bar color
   */
  loading: {
    color: '#3B8070'
  },
  /*
   ** Build configuration
   */
  build: {
    vendor: ['velocity-animate'],
    /*
     ** Run ESLint on save
     */
    extend (config, ctx) {
      config.module.rules.push({
        test: /\.(gif|png|jpe?g|svg)$/,
        use: [
          {
            loader: 'image-webpack-loader',
            options: {
              // the webp option will enable WEBP
              webp: {
                quality: 50
              }
            }
          }
        ],
        exclude: /(node_modules)/
      })
    }
  }
}
