import Vue from 'vue'
import NoSsr from 'vue-no-ssr'

Vue.component('vue-no-ssr', NoSsr)
