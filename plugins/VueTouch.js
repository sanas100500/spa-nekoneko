import Vue from 'vue'
import VueTouch from 'vue2-touch-events'

Vue.use(VueTouch, {
  tapTolerance: 100,
  swipeTolerance: 30
})
