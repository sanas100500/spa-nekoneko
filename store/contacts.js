export const state = () => ({
  telega: '',
  insta: '',
  fb: '',
  vk: '',
  email: '',
  skype: '',
  phone: '',
  copyright: ''
})

export const mutations = {
  setText (state, data) {
    state.telega = data.telega
    state.insta = data.insta
    state.fb = data.fb
    state.vk = data.vk
    state.email = data.email
    state.skype = data.skype
    state.phone = data.phone
    state.copyright = data.copyright
  }
}
