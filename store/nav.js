
export const state = () => ({
  displayNav: false,
  navigation: []
})

export const mutations = {
  setNav (state, data) {
    state.navigation = data.navigation
  },
  showNav (state) {
    state.displayNav = true
  },
  hiddeNav (state) {
    state.displayNav = false
  }
}
