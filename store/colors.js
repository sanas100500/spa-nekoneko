export const state = () => ({
  color: '#fff',
  colorMenuList: 'rgb(70, 128, 255)',
  colorNav: {
    title: '#fff',
    item: 'rgb(70, 128, 255)'
  }
})

export const mutations = {
  setBlue (state) {
    state.color = 'rgb(70, 128, 255)'
    state.colorMenuList = '#fff'
  },
  setWhite (state) {
    state.color = '#fff'
    state.colorMenuList = 'rgb(70, 128, 255)'
  },
  setColorNav (state, arr) {
    state.colorNav.title = arr[0]
    state.colorNav.item = arr[1]
  }
}
