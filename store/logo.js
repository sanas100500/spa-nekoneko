export const state = () => ({
  displayLogo: false
})

export const mutations = {
  showLogo (state) {
    state.displayLogo = true
  },
  hiddeLogo (state) {
    state.displayLogo = false
  }
}
