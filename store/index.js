import axios from 'axios'

export const actions = {
  async nuxtServerInit ({commit}) {
    const {data} = await axios.get('http://localhost:8000/api/landing')
    this.commit('nav/setNav', data)
    this.commit('toltip/setText', data)
    this.commit('contacts/setText', data)
  }
}
