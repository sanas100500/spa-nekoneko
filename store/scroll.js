export const state = () => ({
  isEnd: true
})

export const mutations = {
  animationStart (state) {
    state.isEnd = false
  },
  animationEnd (state) {
    state.isEnd = true
  }
}
