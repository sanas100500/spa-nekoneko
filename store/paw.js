export const state = () => ({
  isThemeNeon: false
})

export const mutations = {
  themeChange (state) {
    state.isThemeNeon = !state.isThemeNeon
  }
}
